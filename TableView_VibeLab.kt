package com.example.tableview_vibelab

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.tableview_vibelab.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var bindingClass : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)
    }

    fun onClickTvButton(view: View){
        var randomVal = Random()
        var color = Color.argb(255,randomVal.nextInt(256), randomVal.nextInt(256),
            randomVal.nextInt(256))

        bindingClass.tvText.visibility = View.GONE
        bindingClass.tvButton.setBackgroundColor(color)
        bindingClass.imPhoto.visibility = View.VISIBLE

        when((0..4).random()){
            1 -> bindingClass.imPhoto.setImageResource(R.drawable.photo1)
            2 -> bindingClass.imPhoto.setImageResource(R.drawable.photo2)
            3 -> bindingClass.imPhoto.setImageResource(R.drawable.photo3)
            4 -> bindingClass.imPhoto.setImageResource(R.drawable.photo4)
        }


    }
}
